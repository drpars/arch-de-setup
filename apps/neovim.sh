#!/bin/bash

# neovim packer
git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim

# additional packages
sudo pacman -S --needed --noconfirm luarocks ruby rubygems composer php julia cpanminus ripgrep


# to install xclip (X11) - wl-clipboard (Wayland)
sudo pacman -S --needed --noconfirm wl-clipboard
sudo pacman -S --needed --noconfirm xclip
