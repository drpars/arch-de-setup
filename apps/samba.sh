#!/bin/bash

user=$(whoami)

sudo cp /home/$user/arch-de-setup/config/smb.conf /etc/samba/
sudo systemctl enable --now smb.service nmb.service
sudo mkdir /var/lib/samba/usershares
sudo groupadd -r sambashare
sudo chown root:sambashare /var/lib/samba/usershares
sudo chmod 1770 /var/lib/samba/usershares
sudo gpasswd sambashare -a $user
sudo smbpasswd -a $user
