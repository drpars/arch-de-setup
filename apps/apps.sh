#!/bin/bash
. ./lib

# Office
sudo pacman -S --needed --noconfirm libreoffice-fresh libreoffice-fresh-tr
# Internet - chromium
sudo pacman -S --needed --noconfirm firefox firefox-i18n-tr thunderbird-i18n-tr thunderbird wget uget transmission-gtk
# Media Player
# pacman -S --needed --noconfirm smplayer smplayer-skins smplayer-themes
# Partition manager
sudo pacman -S --needed --noconfirm gparted
# VirtaulMachine - libvirt/qemu
$DIR/apps/vm.sh
# java
sudo pacman -S --noconfirm jre-openjdk-headless jre-openjdk jdk-openjdk openjdk-doc openjdk-src npm
# utility
sudo pacman -S --needed --noconfirm zsh zsh-completions fd bat cmatrix neofetch mc piper mlocate psutils
# Python
sudo pacman -S --needed --noconfirm python python-pip python-psutil
# Other
sudo pacman -S --needed --noconfirm go xsel xmlstarlet
# Sensors
sudo pacman -S --needed --noconfirm lm_sensors lib32-lm_sensors i2c-tools
sudo sensors-detect --auto
# Games
sudo pacman -S --needed --noconfirm steam

# AUR apps
# Pamac manager
# yay -S --needed --noconfirm archlinux-appstream-data-pamac pamac-aur
# Openrgb
# yay -S --needed --noconfirm openrgb-bin
# Utility
# stacer-bin startup-settings-git isomaster
# Internet
yay -S --needed --noconfirm webapp-manager
# Driver
yay -S --noconfirm mkinitcpio-firmware
