#!/bin/bash
. ./lib

# Applications
$DIR/apps/apps.sh
$DIR/apps/neovim.sh

# Personalization
$DIR/apps/zsh.sh
$DIR/apps/themes.sh

# For sharing
$DIR/apps/samba.sh