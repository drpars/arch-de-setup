#!/bin/bash
. ./lib

# lightly-git is better than kvantum
yay -S --needed --noconfirm lightly-git
# sudo pacman -S --needed --noconfirm kvantum
sudo pacman -S --needed --noconfirm gtk-engine-murrine
sudo pacman -S --needed --noconfirm qt5-graphicaleffects qt5-quickcontrols2 qt5-svg

# Catppuccin - GTK
git clone https://github.com/Fausto-Korpsvart/Catppuccin-GTK-Theme
mv Catppuccin-GTK-Theme/themes ~/.themes
rm -rf Catppuccin-GTK-Theme/

# Qogir icon
git clone https://github.com/vinceliuice/Qogir-icon-theme
cd Qogir-icon-theme
./install.sh
cd ..
rm -rf Qogir-icon-theme

# Mocu cursor
git clone https://github.com/sevmeyer/mocu-xcursor
cd mocu-xcursor
./make.sh
sudo cp -r $DIR/mocu-xcursor/dist/Mocu-White-Right /usr/share/icons
cd ..
rm -rf mocu-xcursor

# # kvantun themes
# if [ ! -d ~/.config/Kvantum ]; then
#   mkdir ~/.config/Kvantum
# fi
# # Activate kvantum theme
# printf "%s\n" "[General]" "theme=Catppuccin-Mocha-Blue" | tee ~/.config/Kvantum/kvantum.kvconfig
# git clone https://github.com/catppuccin/Kvantum
# cd Kvantum
# cp -r $DIR/Kvantum/src/Catppuccin-Mocha-Mauve-Blue ~/.config/Kvantum
# cp -r $DIR/Kvantum/src/Catppuccin-Mocha-Mauve-Sapphire ~/.config/Kvantum
# cd ..
# rm -rf Kvantum
