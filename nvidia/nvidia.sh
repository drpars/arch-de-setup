#!/bin/bash
. ./lib

# if the kernel is "...-lts", install nvidia-lts
# wayland support not enough nvidia-open-dkms - 530.41.03-9

read -n1 -rep "Do you want to install NVIDA driver? (y/n) " answr
if [[ $answr =~ ^[Yy]$ ]]; then
	read -n1 -rep " Which NVIDIA drivers do you want to install Wayland(1) or X11(2) ? (1,2)" nvd
	if [[ $nvd == "1" ]]; then
		echo "NVIDIA drivers are installing for wayland..."
		# no need for manual compilation anymore
		# $DIR/nvidia/nvidia52.sh
		sudo pacman -S --needed --noconfirm libva
		yay -S --needed --noconfirm nvidia-525xx-dkms nvidia-525xx-utils lib32-nvidia-525xx-utils opencl-nvidia-525xx lib32-opencl-nvidia-525xx nvidia-525xx-settings libva-nvidia-driver-git
		if [ $? -ne 0 ]; then
			echo -e "$PKG1 install had failed, please check the install.log"
			exit 1
		fi
		# NVIDIA modules in mkinitcpio
		sudo sed -i "s/MODULES=()/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm kvm kvm_intel)/g" /etc/mkinitcpio.conf
		sudo mkinitcpio -P
		# Additional steps
		sudo echo -e "options nvidia-drm modeset=1" | sudo tee -a /etc/modprobe.d/nvidia.conf
		# Blacklist nouveau
		sudo echo -e "blacklist nouveau" | sudo tee -a /etc/modprobe.d/nouveau.conf
	else
		echo "NVIDIA drivers are installing for X11..."
		sudo pacman -S --needed --noconfirm nvidia-open-dkms nvidia-utils lib32-nvidia-utils opencl-nvidia lib32-opencl-nvidia nvidia-settings libva
		if [ $? -ne 0 ]; then
			echo -e "$PKG1 install had failed, please check the install.log"
			exit 1
		fi		
		# NVIDIA modules in mkinitcpio
		sudo sed -i "s/MODULES=()/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm kvm kvm_intel)/g" /etc/mkinitcpio.conf
		sudo mkinitcpio -P
		# Additional steps
		sudo echo -e "options nvidia-drm modeset=1" | sudo tee -a /etc/modprobe.d/nvidia.conf
		# Blacklist nouveau
		sudo echo -e "blacklist nouveau" | sudo tee -a /etc/modprobe.d/nouveau.conf
	fi
else
	echo "NVIDIA driver installation is aborted..."
fi