#!/bin/bash
. ./lib

# !!!!!run without sudo privilege!!!!!
# !!!!!select desktop environment!!!!!

# Update repository
sudo pacman -Sy

# Make executable
chmod +x $DIR/apps/*.sh
chmod +x $DIR/config/*.sh
chmod +x $DIR/de/*.sh
chmod +x $DIR/nvidia/*.sh

main_menu

# AUR helper - yay
# $DIR/apps/yay.sh
# Nvidia driver
# $DIR/nvidia/nvidia.sh
# Applications
# $DIR/apps/setup.sh

# DesktopEnvironment
# $DIR/de/qtile.sh
# $DIR/de/hyprland.sh
# $DIR/de/plasma.sh
# $DIR/de/gnome.sh
# $DIR/de/xfce.sh

# Systemd bootloader hibernation settings
# sudo $DIR/config/hibernation.sh

# Time settings
# sudo timedatectl set-local-rtc 0 --no-ask-password
# Keyboard settings - x11
# sudo localectl set-x11-keymap tr pc105 --no-ask-password

# systemctl --user enable pipewire-pulse.service
# sudo systemctl start fstrim.service
