#!/bin/bash
. ./lib

read -n1 -rep "Wolud you like to install Qtile WM ? (y,n) " qtl
if [[ $qtl =~ ^[Yy]$ ]]; then
	echo "Qtile WM is installing..."

	awk 'NR==1, NR==7 {print $0}' $DIR/pkg-files/qtile.txt >stg1
	awk 'NR==9, NR==13 {print $0}' $DIR/pkg-files/qtile.txt >stg2
	awk 'NR==15, NR==34 {print $0}' $DIR/pkg-files/qtile.txt >stg3
	awk 'NR==36, NR==38 {print $0}' $DIR/pkg-files/qtile.txt >stg4
	awk 'NR==40, NR==46 {print $0}' $DIR/pkg-files/qtile.txt >stg5
	# stage 1
	while read line; do
		echo "Installing : ${line}"
		install_package ${line}
	done <stg1
	# stage 2
	while read line; do
		echo "Installing : ${line}"
		install_package ${line}
	done <stg2
	# stage 1
	while read line; do
		echo "Installing : ${line}"
		install_package ${line}
	done <stg3
	# stage 1
	while read line; do
		echo "Installing : ${line}"
		install_package ${line}
	done <stg4
	rm stg*

	systemctl enable sddm.service
else
	echo "Qtile WM installation is aborted..."
fi