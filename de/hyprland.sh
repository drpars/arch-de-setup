#!/bin/bash
. ./lib

read -n1 -rp "Would you like to start Hyprland installation and install neccesery packages? (y/n) " hyprins
echo
if [[ $hyprins =~ ^[Yy]$ ]]; then
	# Hyprland and additional packages
	echo "Hyprland installation started..."
	# sudo pacman -S --needed --noconfirm hyprland
  	yay -S --needed --noconfirm hyprland-nvidia
	#
	sudo pacman -S --needed --noconfirm qt5ct qt5-wayland qt6ct qt6-wayland
	yay -S --needed --noconfirm sddm-git waybar-hyprland-git nwg-look-bin
	#
	sudo pacman -S --needed --noconfirm kitty wofi mako swappy grim slurp swaybg playerctl polkit-kde-agent jq python-requests
	yay -S --needed --noconfirm swww swaylock-effects swayidle wlogout
	#
	sudo pacman -S --needed --noconfirm thunar thunar-volman thunar-archive-plugin dolphin dolphin-plugins krusader tumbler ffmpegthumbs file-roller imv mousepad pamixer pavucontrol mpv blueman network-manager-applet btop
	yay -S --needed --noconfirm cava
	#
	sudo pacman -S --needed --noconfirm ttf-jetbrains-mono-nerd ttf-jetbrains-mono ttf-fantasque-sans-mono adobe-source-code-pro-fonts noto-fonts-emoji ttf-fantasque-nerd ttf-ubuntu-nerd ttf-ubuntu-mono-nerd ttf-hack-nerd ttf-firacode-nerd
	# XDG-DESKTOP
	read -n1 -rep "Wolud you like to install XDG-desktop-portal for Hyprland (y/n) " xdg_hypr
	if [[ $xdg_hypr =~ ^[Yy]$ ]]; then
		printf "Installing xdg-desktop-portal-hyprland"
		sudo pacman -S --needed --noconfirm xdg-desktop-portal-hyprland
		sudo pacman -R --noconfirm xdg-desktop-portal-gnome xdg-desktop-portal-gtk
	else
		echo "XDG-desktop-portal for Hyprland won't install"
	fi
else
	echo "Hyprland and required packages not installed..."
fi

# Config files
read -n1 -rep "Would you like to copy config files and wallpapers? (y,n)" CFG
if [[ $CFG =~ ^[Yy]$ ]]; then
	echo "Copying config files..."
	git clone https://gitlab.com/drpars/dotfiles.git
	cp -r $DIR/dotfiles/config/wayland/{hypr,mako,scripts,swaylock,waybar,wlogout,wofi} $DIRUSER/.config/
	cp $DIR/dotfiles/.zshrc $DIRUSER/
	cp $DIR/dotfiles/.gtkrc-2.0 $DIRUSER/
  	cp $DIR/dotfiles/config/kdeglobals $DIRUSER/.config/
	cp -r $DIR/dotfiles/config/{btop,gtk-3.0,imv,kitty,mc,Mousepad,nwg-look,qt5ct,Thunar,xfce4,xsettingsd} $DIRUSER/.config/
	cp $DIR/dotfiles/scripts/* $DIRUSER/.config/scripts/
	# make executable scripts files
	chmod +x $DIRUSER/.config/scripts/*
	chmod +x $DIRUSER/.config/scripts/waybar/*
	chmod +x $DIRUSER/.config/hypr/xdg-portal-hyprland
	# wallpapers
	cp -r $DIR/dotfiles/Wallpaper $DIRUSER/Resimler/
else
	echo "No Config files are copied"
fi

# Set up SDDM
read -n1 -rep "Would you like to set SDDM? (y,n)" SDDM
if [[ $SDDM =~ ^[Yy]$ ]]; then
	echo -e "Setting up the login screen."
	sudo systemctl enable sddm.service
	sddm_conf_dir=/etc/sddm.conf.d
	if [ ! -d "$sddm_conf_dir" ]; then
		printf "$sddm_conf_dir not found, creating...\n"
		sudo mkdir "$sddm_conf_dir"
		echo -e "[Theme]\nCurrent=sugar-candy" | sudo tee "$sddm_conf_dir/10-theme.conf"
		sudo cp $DIR/dotfiles/config/wayland/extra/sddm/sddm.conf "$sddm_conf_dir/"
	fi
	echo -e "Setting up sugar-candy theme."
	sugar_candy_dir=/usr/share/sddm/themes/sugar-candy
	if [ ! -d "$sugar_candy_dir" ]; then
		printf "$sugar_candy_dir not found, creating...\n"
		sudo tar -xzvf $DIR/dotfiles/config/wayland/extra/sddm/sugar-candy/sugar-candy.tar.gz -C /usr/share/sddm/themes/
		sudo cp $DIR/dotfiles/config/wayland/extra/sddm/sugar-candy/theme.conf "$sugar_candy_dir/"
		sudo cp $DIR/dotfiles/config/wayland/extra/sddm/sugar-candy/background.jpg "$sugar_candy_dir/Backgrounds/"
	fi
else
	echo "SDDM isn't set"
fi
