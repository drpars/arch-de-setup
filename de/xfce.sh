#!/bin/bash

sudo pacman -S --needed xfce4 xfce4-goodies lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings plank pavucontrol --noconfirm
sudo pacman -S gnome-system-tools alacarte mugshot materia-gtk-theme --noconfirm

sudo systemctl enable lightdm.service
