#!/bin/bash

part1=$(lsblk -dno PARTUUID /dev/nvme0n1p3)
part2=$(findmnt -no UUID -T /swapfile)
part3=$(filefrag -v /swapfile | awk '$1=="0:" {print substr($4, 1, length($4)-2)}')

cat > /boot/loader/entries/arch.conf << EOF
title   Arch Linux
linux   /vmlinuz-linux-zen
initrd  /intel-ucode.img
initrd  /initramfs-linux-zen.img
options root=PARTUUID=$part1 rw rootfstype=ext4 systemd.unit=graphical.target nvidia-drm.modeset=1 resume=UUID=$part2 resume_offset=$part3
EOF
